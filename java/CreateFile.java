public class CreateNewFile {

    public static void createFile(String fullPath) throws IOException {
        File file = new File(fullPath);
        file.getParentFile().mkdirs();
        file.createNewFile();
    }

    public static void main(String [] args) throws Exception {
        String path = "C:/donkey/bray.txt";
        createFile(path);
    }

}
