package s2.thread;

public class JoinAndSleep extends Thread {
    /**
     * Der Thread auf dessen Beendigung gewartet wird
     */
    Thread previousThread;

    /**
     * Zeit in Millisekunden zum Schlafen
     */
    int sleep;

     /**
     * Der Konstruktor
     * @param time Die Zeit in ms die der Thread schlafen soll
     * @param previousThread der Thread auf den gewartet werden soll bis er beendet ist
     */
    public JoinAndSleep(int time, Thread thread) {
        sleep = time;
        previousThread = thread;

        System.out.println("Thread: " + this + " erzeugt");
    }

     /**
     * Die überschriebene Methode run() sie führt den Code aus
     */
    public void run() {
        System.out.println("Thread: " + Thread.currentThread() + " gestartet");

        try {
            if (previousThread != null) {
                previousThread.join();

                System.out.println("Thread: " + Thread.currentThread()
                        + " join auf " + previousThread + " fertig");
            }
        } catch (InterruptedException e) {

        }

        System.out.println("Thread: " + Thread.currentThread()
                + " schlaeft jetzt fuer " + sleep + "ms");

        try {
            Thread.sleep(sleep);
        } catch (InterruptedException e) {

        }

        System.out.println("Thread: " + Thread.currentThread() + " endet");
    }

     /**
     * Das Hauptprogramm
     * @param args keine Parameter benötigt!
     */
    public static void main (String[] args) {
        JoinAndSleep s3 = new JoinAndSleep(2003, null);
        JoinAndSleep s2 = new JoinAndSleep(2002, s3);
        JoinAndSleep s1 = new JoinAndSleep(2001, s2);

        s1.start();
        s2.start();
        s3.start();
    }
}
