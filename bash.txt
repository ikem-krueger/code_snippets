blkid /dev/sdc # get partition type (dos -> MBR)
pkexec xfpm-power-backlight-helper --set-brightness 35 # change display brightness to 35%
printf "%s" "$(< bad.txt)" > good.txt # removing all trailing newlines
patchelf --set-interpreter /tmp/sysroot/lib/ld-linux-x86-64.so.2 ./hello_c
dkms status # list packages managed by dkms
sudo lsof -i -P -n | grep LISTEN # list open ports and their associated applications
stat -c '%a' /etc/passwd # get octal permissions
fold -w 80 /dev/vcs2 > screendump.txt # get text from tty2
ps -e -o pid,tty,cmd | awk '$2~/tty/' | sort -k 2 # list all open tty
sort -k 2 file.txt # sort by column #2
sort -k 2,2 -k 1,1 file.txt # sort on more than one column
memstat | sort -k 1 -t':' -g -r | less # show processes virt memory, sorted by usage
iozone -a -i 2 -r 4 # 4k random read/write test
ps --ppid 2 -p 2 --deselect # show running tasks like htop
who -u # list user session pids
diff /usr/share/base-files/profile /etc/profile
strace <binary>
GTK_THEME=<theme-name> <application> # run single application with a different gtk theme
xdpyinfo | grep dots # get dpi
gtk-update-icon-cache -f -t /usr/share/icons/Mint-Y-Sand
xdg-desktop-menu forceupdate
sgdisk --backup=/mnt/sdc.gpt.header.backup /dev/sdc # backup gpt partition table
sgdisk --load-backup=/mnt/sdc.gpt.header.backup /dev/sdc # restore gpt partition table
sfdisk -d /dev/sda > sda.partition.table.12-30-2015.txt # backup mbr partition table
sfdisk /dev/sda < sda.partition.table.12-30-2015.txt # restore mbr partition table
echo | openssl s_client -connect 'example.com:853' # check dns resolver
kdig @example.com ct.de +tls # check dns resolver
date -u "+%Y%m%dT%H%M%S%4NZ"
ls -Z # parse SELinux ACLs and display them in a readable format
find /path/to/dir -empty -type d -delete # delete all empty directories
new_name=${old_name%.old_suffix}.new_suffix # replace file extension
echo "somepassword"|tee -|smbpasswd -s # set samba user password
iconv -f utf-16le -t utf-8 file.txt > newfile.txt # convert cmd output to bash output encoding
watch -n1 vcgencmd measure_clock arm
watch -n1 vcgencmd measure_temp
ssh-copy-id pi@raspberrypi2.local # copy ssh public key to host
xclip -selection clipboard < ~/.ssh/id_edxxxx.pub # copy ssh public key into clipboard
findmnt -t btrfs
xpra start-desktop ssh://user@host/ --resize-display="1600x900" --exit-with-children --start-child=startxfce4
csplit file.txt --prefix="" --suffix-format="%03d.csv" --elide-empty-files --suppress-matched /----/ "{*}"
grep -rl oldtext .|xargs sed -i 's/oldtext/newtext/g' # recursive replace a string
tr < file-with-nulls -d '\000' > file-without-nulls # remove null characters
scp -r root@IP:/path/to/file /path/to/filedestination
smbclient -L host # list samba shares and current users
smbclient -L ip_of_net_interface -U your_user_name
find . -iname "WSFY321.c" # find a file case insensitive
curl -sL https://api.github.com/users/ikem-krueger/repos?per_page=100|jq -r '.[]|.clone_url'|xargs -L1 git clone
curl -sL "https://gitlab.com/api/v4/users/ikem-krueger/projects/?page=1&per_page=100"|jq -r '.[]|.http_url_to_repo'|xargs -L1 git clone
pkexec env DISPLAY=$DISPLAY XAUTHORITY=$XAUTHORITY gedit # replacement for "gksu"
smartctl -t short /dev/<device>
smartctl -t long /dev/<device>
smartctl -t conveyance /dev/<device>
pv -tpreb /dev/mmcblk0p2 | dd of=/dev/sda1 bs=1M
xinput list
xinput set-prop 'Logitech USB-PS/2 Optical Mouse' 'libinput Natural Scrolling Enabled' 1 # enable natural scrolling
cat /proc/net/wireless
grep "Codec" /proc/asound/card*/codec* # get used audio codec
lspci -nnk|grep -i "net" # show vendor and product id of the network card
head -c 10 file.txt # get the first ten bytes of a file
cat -s file # strip consecutive empty lines
getfacl -R . > permissions.facl
setfacl --restore=permissions.facl
tree -p # show folder tree with permissions
pdftk input.pdf cat output tmp.pdf uncompress; strings tmp.pdf|grep -i "\/URI (" # extract all links from a pdf file
arp -n # show all devices in the network
diff --strip-trailing-cr file1 file2
rsync -n -avxAHXS <src-subvol> <dst-subvol> # make sure that the cloning actually produced a 1:1 copy
fsck.ext4 -C 0 -p /dev/sda1 # fsck with progress bar, and auto repair
tune2fs -O dir_nlink /dev/sda1 # enable "dir_nlink" feature
tune2fs -r 0 /dev/sda1 # set reserved blocks to 0 blocks
tune2fs -m 0 /dev/sda1 # set reserved blocks to 0 percent
echo "offline">/sys/block/sda/device/state # set device state to offline
echo "1">/sys/block/sda/device/delete # delete device
less .node_repl_history
aptitude -O installsize -F'%p %I' search '~i' # show installed packages by size
[ -d /run/systemd/system ] && echo "systemd is running..."
smbpasswd -a theusername
gio mount smb://workgroup;admin@fritz-nas/fritz.nas/Daten
cat /dev/urandom | gzip -9 > /dev/null # cpu load
dd if=/dev/zero of=loadfile bs=1M count=1024 # harddisk load
ssh user@remote-host "cat path/file.name"|diff path/file.name -
fdisk -l -u /dev/sdb # pick last partitions end
dd if=/dev/sdb of=disk.img count=<end+1> status=progress
fdisk -l -u /path/disk.img # pick partitions start
mount -o loop,offset=<start*512> /path/disk.img /mnt/loop
swaplabel -U <uuid> /dev/sdb3
tune2fs -U random /dev/sdb1
/sys/firmware/efi # this folder exist only, if efi is used for booting
fstransform /dev/sda1 btrfs --force-untested-file-systems # convert ext4 to btrfs
mount -t btrfs -o subvolid=5 /dev/mmcblk0p2 /mnt # mount top level root
mount -t btrfs -o subvolid=821 /dev/mmcblk0p2 /mnt # mount subvolume by id
mount -t btrfs -o subvol=root/@btrfs-auto-snap_daily-2021-01-01-1002 /dev/mmcblk0p2 /mnt # mount subvolume by label
mkvmerge --chapters chapters.txt -o output.mkv input-file.mkv # add chapters to mkv file
curl --data "My Data" http://<my-ip>:80
cat /sys/module/cfg80211/parameters/ieee80211_regdom
logout
dmidecode -t 16 # determine the motherboards maximum memory capability
systemctl list-unit-files --state=enabled --no-pager # check on the enabled services and daemons
ip addr show
ethtool -P eth0 # show hardware mac address
wpa_cli -i wlan0 wps_pbc # trigger wps push button configuration
wpa_cli -i wlan0 wps_cancel # cancel wps push button configuration
cat /sys/firmware/devicetree/base/model # show the raspberry pi hardware version
pinout # show the raspberry pi gpio layout
gpio readall
install -d -m 1777 -o root -g root /home/tmp # create directory with certain mode, user, group
stat /tmp # get octal permissions for "/tmp"
passwd -d username # delete password for username
watch df -h # repeat a command at a set interval
renice 20 -u user_name # renice all running processes for a specific user
ncdu -x / # find big files and folders
aptitude -O installsize -F'%p %I' search '~i' # show installed packages by size
mkfs.ext4 -i 8192 /dev/sdc1 # the lower the inode ratio, the more files you can create in your file system
mkfs.ext4 -c -c -v /dev/sdc1
mkfs.ext4 -c -v /dev/sdc1
badblocks -s -v -v -n -c 262144 /dev/sda # read 64 blocks to check
dd_rescue -o sdb1.badblocks -b 256k -B 256k /dev/sdb1 /dev/null
ifconfig -m re0
ifconfig re0 media 1000baseTX mediaopt full-duplex
ifconfig re0 media autoselect
service xbmc stop # stop kodi
service xbmc start # start kodi
sysbench --test=cpu --cpu-max-prime=20000 --num-threads=8 run
rpi-update
udisksctl loop-setup -r -f image.iso
udisksctl loop-delete -b /dev/loop0
mount -t cifs -o vers=1.0,credentials=/root/.smbcredentials //fritz.box/FRITZ.NAS/Daten /media/Daten
mount -t cifs -o user=testuser,domain=testdomain //192.168.1.100/freigabe /mnt
woeusb -d Win10_1809Oct_v2_German_x64.iso /dev/sdb --tgt-fs ntfs # create windows 10 usb installation stick
lsblk -f # show partition infos
ls /dev/pts/ # list all used virtual terminals
arp-scan --interface=eth0 --localnet # list all connected ips in the network
host <ip> # get hostname from ip
arecord -f cd -|aplay -
arecord -f cd -|tee output.wav|aplay -
cat /proc/asound/cards # check soundcard for input/output devices
v4l2-ctl -list-ctrls
dmidecode --type memory # get ram informations
lpstat -p -d # get stats for the local printer
lpr -o fit-to-page -o media=A4 -P Brother_DCP-7055 *.pdf # print pdfs on the local printer
xinput # list devices and their ids
xinput set-prop <id> "Evdev Scrolling Distance" -1 -1 -1 # enable natural scrolling
curl -O http://www.openss7.org/repos/tarballs/strx25-0.9.2.1.tar.bz2 # wget like download of a file
cat /sys/firmware/acpi/tables/SSDT>SSDT.aml
vimtutor
cryptsetup luksOpen /dev/sdb2 fedora_khaleesi-home
mount /dev/mapper/fedora_khaleesi-home /media/fedora_khaleesi-home
awk '$1 ~ /^Line:/ {print $2}' <file>
awk 'BEGIN{print ENVIRON["X"], ENVIRON["TERM"]}'
lt --port 80 --subdomain myubuntu # access the local web server from https://myubuntu.localtunnel.me
mount -t ntfs-3g -o remove_hiberfile /dev/sda5 /media/92441EC8441EAF4B 
from pylab import * # import the whole numpy-scipy-matplotlib stack
ls -v # list files numerically sorted
pinout # see a handy gpio pinout diagram
setxkbmap -types local -print
shred -vuzn 0 <file> # overwrite and remove file
getcap /usr/bin/ping # check the default capabilities of the ping executable
fc-list # list installed fonts
tune2fs -l /dev/<device>|grep "Default mount options"
type $COMMAND # display information about command type
whatis $COMMAND # display one-line manpage description
lsusb -t # list usb device hierarchy as a tree
python -m http.server # python 3.x webserver
python -m SimpleHTTPServer # python 2.x webserver
./configure -C # autoconf caching
GTK2_RC_FILES=/path/to/theme/gtkrc <command> # run an application with a different gtk theme
xrandr --output LVDS1 --mode 1024x600 --panning 1280x768 --scaling 1.28x1.28
chmod --reference=<source> <destination> # copy permissions from one file to another
chown --reference=<source> <destination> # copy ownership from one file to another
DISPLAY=":0" XAUTHORITY="$HOME/.Xauthority" vlc # start vlc from virtual console
php -S localhost:8000
mp3splt -c music.cue music.mp3
strace -p $PID -f -e trace=network -s 10000 # monitor an existing process with a known pid
gsettings set org.mate.interface gtk-enable-animations false
pip list # list installed packages
python3 -m venv # create virtual environment
upower -i /org/freedesktop/UPower/devices/battery_BAT0
usermod -l <new_username> -m -d /home/<new_username> <old_username> # rename a user
groupmod -n <old_group> <new_group>
xset dpms force off # turn off the lcd backlight
ddccontrol -p # alter settings equivalent to the hardware buttons on the monitor
xset led 3 # turn on keyboard backlight
xset led off # turn off keyboard backlight 
hdparm -tT /dev/sda # measure hard disk data transfer speed
hdparm -I /dev/sda|grep -i speed # see sata link speed options
dmesg|grep -i sata|grep 'link up' # see sata link speed used
xmllint --format ugly.xml # prettify (indent) xml files
gcc -S file.c # convert a c language file to assembly
nbd-client -d /dev/nbd0'
modprobe <module> <parameter>=<value>
modprobe -c <module> # dump out the effective configuration
count=‘expr $count + 1‘
update-mime-database /usr/share/mime 
grub2-mkconfig -o /boot/grub2/grub.cfg
grub2-mkconfig -o /boot/efi/EFI/fedora/grub.cfg
modinfo -p <module> # print possible module parameters
hdparm -r0 /dev/sdb # turn off the disk devices write protect
wkhtmltopdf http://www.cyberciti.biz/path/to/url.html output.pdf # convert a html page to a pdf
vi /etc/rc.local.shutdown
wpa_passphrase <ssid> <psk>
wpa_supplicant -h # list of valid driver values
locale charmap # query the name of the character encoding in your current locale with the command
locale -m # provide a list with the names of all installed character encodings
limit -t 600s -m 1000000k <command> <arguments> # limit time and memory consumption of a program
dconf reset -f / # reset desktop settings to default
readelf -h /bin/uname
strace -e trace=open <program> # print all fopen-calls sent to the system
localedef -f ISO-8859-1 -i en_US.new en_NEW # translate en_US.new and safe the locale as en_NEW
sort -o <file> <file> # safely sort a file in place
exec python script.py # replace shell process
find . -name "*.js" -not -path "directory/*" # exclude directory from find command
genisoimage -o ../freedos_biosupdate.iso -q -l -N -boot-info-table -iso-level 4 -no-emul-boot -b isolinux/isolinux.bin .
nm <file> # list the symbols inside an object file (.o) or a (static) library (.a)
ldd <file> # list the dynamic libraries this library (.so) or executable binary depends on
ldd -Ur <file> # try to process all relocations and dynamic linking and output errors
objdump -x <file> # dump the content of the object file, library, or executable
readelf -a <file> # parse the internal structures of ELF files (.o, .so, .sl, executables) and dump them
elfdump <file>
lsof -P -T -p <pid>
fuser -v /dev/snd/*
gcore <pid> # dump the memory of the process
autopep8 --in-place --aggressive --aggressive <filename> # auto format python code to conform to the pep 8 style guide
pip install pypiwin32 # install the package for the python win32api module
lame --preset standard file.wav # convert wav to mp3
backportpackage -s preci
