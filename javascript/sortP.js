function sortP() {
    Array.from(document.body.getElementsByTagName('p'))
        .sort((a, b) => a.textContent.localeCompare(b.textContent))
        .forEach(p => document.body.insertBefore(p, sort))
}

document.getElementById("sort").addEventListener('click', sortP, false)
