You can do it in `screen` the terminal multiplexer.

* To split vertically: <kbd>ctrl</kbd><kbd>a</kbd> then <kbd>|</kbd>.
* To split horizontally: <kbd>ctrl</kbd><kbd>a</kbd> then <kbd>S</kbd> (uppercase 's').
* To unsplit: <kbd>ctrl</kbd><kbd>a</kbd> then <kbd>Q</kbd> (uppercase 'q').
* To switch from one to the other:  <kbd>ctrl</kbd><kbd>a</kbd> then <kbd>tab</kbd>

Note: After splitting, you need to go into the new region and start a new session via <kbd>ctrl</kbd><kbd>a</kbd> then <kbd>c</kbd> before you can use that area.

EDIT, basic screen usage:

* New terminal: <kbd>ctrl</kbd><kbd>a</kbd> then <kbd>c</kbd>.
* Next terminal: <kbd>ctrl</kbd><kbd>a</kbd> then <kbd>space</kbd>.
* Previous terminal: <kbd>ctrl</kbd><kbd>a</kbd> then <kbd>backspace</kbd>.
* N'th terminal <kbd>ctrl</kbd><kbd>a</kbd> then <kbd>[n]</kbd>. *(works for n∈{0,1…9})*
* Switch between terminals using list: <kbd>ctrl</kbd><kbd>a</kbd> then <kbd>"</kbd> *(useful when more than 10 terminals)*
* Send <kbd>ctrl</kbd><kbd>a</kbd> to the underlying terminal <kbd>ctrl</kbd><kbd>a</kbd> then <kbd>a</kbd>.
