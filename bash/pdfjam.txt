pdfjam file1.pdf '-' file2.pdf '1,2' file3.pdf '2-' --outfile output.pdf # merge pdf files
pdfjam file1.pdf '1,2' --outfile first.pdf # split pdf file
pdfjam file1.pdf '3-' --outfile second.pdf
pdfjam --trim '1cm 2cm 1cm 2cm' --clip true file1.pdf --outfile output.pdf # trim pdf file
pdfjam --nup 2x2 file1.pdf --outfile output.pdf