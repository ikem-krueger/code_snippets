btrfs-auto-snapshot list root
btrfs-auto-snapshot rollback root/@btrfs-auto-snap_daily-2014-06-04-1905
btrfs-auto-snapshot destroy root/@btrfs-auto-snap_daily-2014-06-04-1905
btrfs-auto-snapshot list | grep -v /@$ | grep auto-snap | xargs -L1 btrfs-auto-snapshot destroy # remove all snapshots at once
