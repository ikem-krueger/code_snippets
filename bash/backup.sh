#!/bin/bash
#Juergen Donauer
#http://www.bitblokes.de
#Freeware!
#Benutzung auf eigene Gefahr!


notify_me() {
  ALL_USER="$(users | sort)"
  echo Desktop-Message to: $ALL_USER
        for USER_TMP in $ALL_USER; do
                if [ "$USER_TMP" = "$USER_LAST" ]; then
                        #if user already notified, continue
                        continue
                fi
                su $USER_TMP -c "XAUTHORITY=/home/$USER_TMP/.Xauthority DISPLAY=:0 notify-send $1 $2"
                #save last username to avoid double notification
                USER_LAST=$USER_TMP
        done
}


########## Konfiguration ############
#UUID="42d23002-497e-4ed9-9697-057a643108b8" #ist durch die eigene UUID zu ersetzen
UUID="1114-2056"
CHECK_DISC="/dev/disk/by-uuid/$UUID"
DATE=`/bin/date +%Y%m%d`
CHECK_DATE_DIR="/root" #Verzeichnis der Datum-Steuerdatei und temporäres Einbinden der Backup-Platte
BACKUP_DIR_MEDIA="backup" #Mounte die Backup-Platte da hin
BACKUP_DIR="$CHECK_DATE_DIR/$BACKUP_DIR_MEDIA" #Backup-Verzeichnis, in dem Fall root der Backup-Platte
DATE_FILE="$CHECK_DATE_DIR/$DATE.date" #Datum-Steuerdatei, ob Backup schon durchgeführt wurde
CHECK_RSYNC=`ps -aef | grep -v grep | grep rsync | wc -l`
CHECK_MOUNT=`df -h | awk '{print $6}' | grep $BACKUP_DIR_MEDIA | wc -l`
SOURCE_DIR="/zu/sicherndes/verzeichnis/"

#####################################

/bin/umount $CHECK_DISC

if [ ! -d $CHECK_DATE_DIR/$BACKUP_DIR_MEDIA ]; then
	/bin/mkdir $CHECK_DATE_DIR/$BACKUP_DIR_MEDIA
fi

if [ $CHECK_RSYNC = 0 ]; then #wenn kein anderer rsync-Prozess läuft, dann leg los
	if [ -e $CHECK_DISC ]; then
		if [ $CHECK_MOUNT = 0 ]; then
			if [ ! -f $DATE_FILE ]; then
				/bin/mount UUID=$UUID $CHECK_DATE_DIR/$BACKUP_DIR_MEDIA
				sleep 5
				rm -f $CHECK_DATE_DIR/*.date
				notify_me "Backup\-Platte\ erkannt" "starte\ Datensicherung\ \(ich\ sag\ es\ Dir,\ wenn\ ich\ fertig\ bin\!\)"
				/usr/bin/rsync -avn --safe-links --delete --ignore-errors $SOURCE_DIR $BACKUP_DIR #macht nur einen Testlauf. Für den Ernstfall -avn zu -av ändern
				/bin/sync
				/usr/bin/touch $DATE_FILE
				/bin/umount /dev/disk/by-uuid/$UUID
			else
				notify_me "Backup\ heute\ schon\ durchgefuehrt" "Du\ kannst\ die\ Festplatte\ nun\ abstecken"
			fi
		fi
	fi
fi
