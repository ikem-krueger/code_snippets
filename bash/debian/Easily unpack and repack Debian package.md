Create an empty directory and switch to it:

```
mkdir tmp
```

Then run `dpkg-deb` to extract its control information and the package files:

```
dpkg-deb -R original.deb tmp
```

Beware that unless you're not root, the files' permissions and ownership will be corrupted at the extraction stage.

Use `dpkg-deb -b` to rebuild the package:

```
dpkg-deb -b tmp fixed.deb
```

Rename package:

```
dpkg-name fixed.deb
```
