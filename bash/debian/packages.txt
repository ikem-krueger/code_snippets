manpages: funny-manpages
linux: linux-source kernel-package bin86 fakeroot libncurses5-dev
anchor-cms: php-gd php-mcrypt php-curl php-sqlite3 php-mysql
boot-tools: rtirq-init ksm zram-config ureadahead preload
fglrx-driver: fglrx-driver fglrx-control fglrx-modules-dkms xvba-va-driver
compton-git: libx11-dev libxcomposite-dev libxdamage-dev libxfixes-dev libxext-dev libxrender-dev libxrandr-dev libxinerama-dev pkg-config make x11proto-core-dev libpcre3-dev libconfig-dev libdrm-dev libgl1-mesa-dev libdbus-1-dev asciidoc
top-tools: powertop htop iotop
