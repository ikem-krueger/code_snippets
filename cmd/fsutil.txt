fsutil behavior set DisableDeleteNotify ; check if trim is enabled
fsutil behavior set DisableDeleteNotify 0 ; enable trim
fsutil dirty set c: ; set dirty bit and force a filesystem check
fsutil repair set c: 0
fsutil resource setautoreset true C:\ ; delete ntfs metadata transactions on next mount
