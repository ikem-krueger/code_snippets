bcdedit /delete {current} /f ; delete current boot entry
bcdedit /delete {identifier} ; remove boot entry
bcdedit /deletevalue {default} safeboot
bcdedit /deletevalue useplatformclock
bcdedit /export c:\boot.bcd ; backup bcd entries
bcdedit /import c:\boot.bcd ; restore bcd entries
bcdedit /set {current} description "any name" ; change boot entry name
bcdedit /set {default} bootmenupolicy legacy
bcdedit /set {default} bootmenupolicy standard
bcdedit /set {default} recoveryenabled no ; disable automatic startup repair
bcdedit /set {default} safeboot minimal
bcdedit /set {default} safeboot network
bcdedit /set {default} safebootalternateshell yes
bcdedit /set nointegritychecks on
bcdedit /set recoveryenabled
bcdedit /set testsigning on
bcdedit /set useplatformclock true
bcdedit /store E:\boot\BCD /enum
bcedit /set recoveryenabled no ; disable auto repair
