powercfg /energy
powercfg /batteryreport ; get battery informations
powercfg –report ; calculate battery life time
powercfg -attributes SUB_SLEEP HIBERNATEIDLE -ATTRIB_HIDE ; expose the hibernate idle time-out setting
powercfg -attributes SUB_SLEEP HIBERNATEIDLE +ATTRIB_HIDE ; hide the hibernate idle time-out setting
powercfg -h off ; disable hibernate
powercfg -h on ; enable hibernate
powercfg /a ; check if hibernation is on
