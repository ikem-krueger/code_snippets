Apache and MySQL can be configured so that they **only listen to requests from your own computer**. For most test systems this is fine and it greatly reduces the risk because  the services are not reachable from the Internet.

Before you start XAMPP for the first time find and edit these files:

For **Apache** edit the files xampp\apache\conf\httpd.conf and xampp\apache\conf\extra\httpd-ssl.conf. Look for lines starting with "Listen" such as

    Listen 80

and replace them with 

    Listen 127.0.0.1:80


For **MySQL** open the file xampp\mysql\bin\my.cnf find the section "[mysqld]" and add this line

    bind-address=127.0.0.1


After starting the services, **verify the result** by going to a command window and start and execute:

    netstat -a -n

For the entries marked as LISTEN in the last column, look at the Listen column. It should always start with 127.0.0.1 or ::1 but not with 0.0.0.0.

Sources: 

[1] https://security.stackexchange.com/a/6807/214437
