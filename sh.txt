chmod -R -N ~ # reset acls
repairHomePermissions # repair home permissions in recovery mode
rclone copy --progress source remote:destination
softwareupdate --ignore "macOS Catalina"
softwareupdate --reset-ignored
system_profiler SPHardwareDataType|grep "Model Id"
kextcache -system-prelinked-kernel
kextcache -system-caches
easy_install pip
gunzip filename.xz # extract xz file
sysctl -n hw.ncpu # show the number of logical cores of a cpu
ntfs-3g /dev/disk2s2 /Volumes/Windows -o local,allow_others # mount windows partition using ntfs-3g
chntpw -l sam /Volumes/Windows/Windows/System32/config/sam # list windows user accounts
chntpw -i sam /Volumes/Windows/Windows/System32/config/sam # interactive edit windows user accounts
reged -e /Volumes/Windows/Windows/System32/config/software # interactive edit windows registry
rm /Library/Preferences/SystemConfiguration/*.plist # reset system settings
rm -r /System/Library/Caches/com.apple.kext.caches # delete kextcaches
spctl --master-disable # disable gatekeeper
~/Library/Logs/DiagnosticReports/
zip -r -X archive_name.zip folder_to_compress # create a zip without the invisible resource files "_MACOSX", "._Filename", ".ds_store"
open -a ApplicationName # open an app from the command line
~/Library/Containers/com.apple.BKAgentService/Data/Documents/iBooks/Books
~/Library/Mobile\ Documents/iCloud\~com\~apple\~iBooks/Documents
osascript -e 'id of app "App Name"' # get the bundle identifier for the app
chflags -h hidden "/Applications/[App Name].app" # hide the app
chflags -h nohidden "/Applications/[App Name].app" # unhide the app
time sudo RepairPermissions --verify /
dscl localhost delete /Local/Default/Users/bobross # delete the local account from the user database
atsutil databases -remove # clear the font databases and font caches
rm /var/db/.AppleSetupDone # re-run setup assistant on next reboot
find /private/var/folders/ -name com.apple.dock.iconcache -exec rm {} \; # clear the icon cache
~/Library/Preferences/com.apple.Safari.Extensions.plist
dd bs=512 if=/dev/rXX# of=/some_dir/foo.dmg conv=noerror,sync # recover a dead hard drive using dd 
nvram -x -p>/Volumes/EFI/nvram.plist # generate nvram.plist for use with clover
iotop -oPa
sysctl -a|grep cpu
tmutil snapshot # create an apfs snapshot
tmutil listlocalsnapshots / # check if there are any snapshots
systemsetup -setstartupdisk
bless --info
"/Applications/Install macOS High Sierra.app/Contents/Resources/startosinstall" --applicationpath "/Applications/Install macOS High Sierra.app" --volume / --nointeraction # mac os x in-place upgrade without user interaction
fsck_apfs -n -l -x /dev/rdisk1s1
fsck_apfs -y -x /dev/rdisk1s1
rsync -nrcv ~/Desktop/folder1/ ~/Desktop/folder2/ # verify two mirrored directories
pkgutil --expand ~/Desktop/OSXUpdCombo10.10.2.pkg ~/Desktop/modified
pkgutil --flatten ~/Desktop/modified ~/Desktop/ComboUpdateModified.pkg
csrutil status # check if system integrity protection is enabled
csrutil enable # enable system integrity protection
csrutil disable # disable system integrity protection
diskutil apfs updatePreboot
vsdbutil -d /VolumeName # ignore ownerships on a volume
vsdbutil -a /VolumeName # restore ownerships on a volume
vsdbutil -c /VolumeName # check the status of ownerships on a volume
dd if=/dev/zero of=/dev/null bs=64m count=1000 & while pkill -INFO -x dd; do sleep 1; done
rm /var/db/.applesetupdone # remove the “setup has been completed” file
languagesetup
drutil getinfo # get info for optical drive
drutil poll # constantly poll optical drive for changes
/Library/LaunchDaemons
sysctl -a|grep machdep.cpu
defaults read com.apple.Finder appleshowallfiles
defaults write com.apple.Finder appleshowallfiles true
defaults delete com.apple.Finder appleshowallfiles
swift test # runs tests
swift package generate-xcodeproj # creates an Xcode project
killall -s Humans|awk '{print $3}' # alias for "pidof"
killall -9 -v com.apple.CoreSimulator.CoreSimulatorService
appiconsetgen --iOS appicon_1024.png
rm -P wikileak1.txt # secure delete a single file
rm -Pr ~/.wikileaks # secure delete a folder recursively
resetpassword
defaults write com.apple.Preview NSQuitAlwaysKeepsWindows -bool false
~/Library/Safari/Bookmarks.plist
/Library/Fonts
~/Library/Saved Application State/
defaults write com.apple.loginwindow LoginHook /path/to/script
purge # clear ram
defaults read ~/Library/Preferences/com.apple.LaunchServices/com.apple.launchservices.secure.plist
defaults write com.microsoft.word NSQuitAlwaysKeepsWindows -bool false # disable the saved state for microsoft word
rm -Rf "~/Library/Saved Application State/com.microsoft.word.savedstate" # trash the saved application state for microsoft word
gem install cocoapods
fs_usage
trimforce enable
/System/Library/Frameworks/CoreServices.framework/Versions/A/Frameworks/LaunchServices.framework/Versions/A/Support/lsregister
lsregister -kill -seed # reset the uniform type identifiers
lsregister -kill -r -domain local -domain system -domain user # reset the launch services database
lsregister -dump # dump the database to the screen
lsregister -f /Applications/Xcode.app # force an application to re-register file types
lsregister -u /Applications/Xcode.app # unregister a specific application
find /Library/Extensions/*.kext /System/Library/Extensions/*.kext -maxdepth 0
kextstat|grep -v com.apple # list all third party kernel extensions
kextunload /System/Library/Extensions/NameOfExtension.kext
kextload /System/Library/Extensions/NameOfExtension.kext
bdmesg|grep 'Volume: .*HD([0-9],.*)' # look for drive identifiers for clover
chflags nohidden ~/Library
dspci
swift -I ~/mymodules
echo 'Hello World!'|pbcopy # copy a string to the clipboard
echo $(pbpaste) # show the content of the clipboard
system_profiler SPUSBDataType
rsync -xrlptgoEvHS --progress --delete / /Volumes/BackupClone # create a full copy of the source system
man ascii
mount -t fuse-ext2 /dev/disk0s1 /Volumes/Linux/ # mount ext2/3/4 partition
cat file.txt|say -o file.aiff # text to speech
setpci -s 02:0.0 40.b=b3 41.b=a1 42.b=c2 # enable jmicron jmb363 sata/pata dual mode
defaults write com.apple.SoftwareUpdate ScheduleFrequency -int 1
rm ~/Library/Safari/History.db* # delete safari history
trimforce disable # speed up boot on old macbooks
pdfbooklet source.pdf destination.pdf
defaults write com.apple.screencapture type jpg # change default screenshot file type
defaults write com.apple.SoftwareUpdate ScheduleFrequency -int 1 # set update frequency
defaults write com.apple.desktopservices DSDontWriteNetworkStores true
hdiutil chpass /path/to/the/diskimage # change the password on an encrypted disk image
hdiutil attach example.dmg -shadow /tmp/example.shadow -noverify # mount a read-only disk image as read-write
mount -t hfs /dev/disk1s1 /Volumes/Foo # mount a removable drive
defaults write com.apple.finder ShowAllFiles TRUE # show hidden files in MacOS Finder
mount -uw / # mount "/" read write
lsof -i|grep "LISTEN" # show all listening connections
dscacheutil -flushcache # flush out the dns cache
mkfile 10m 10MB.dat # create a file of a given size (all zeros)
hdiutil create -size 10m 10MB.dmg # create a file of a given size (all zeros)
qlmanage -p $FILE # open a file in MacOS Quicklook Preview
stty sane # restore terminal settings
open -a Finder ./ # open current directory in MacOS Finder
dsenableroot -d # disable root account
afsctool -c -l -k -v -i -9 /Library # transparent file compression
ditto –hfsCompression # transparent file compression
dscl . -append /groups/admin GroupMembership $USER # add user to the admin group on
dscl . -delete /groups/admin GroupMembership $USER # delete user from the admin group
caffeinate -i -t 3600 # prevent pc from going to sleep
