import sys

from base64 import decodestring

output_file = sys.argv[1]
imagestr = sys.argv[2].encode("ascii")

with open(output_file,"wb") as f:
    f.write(decodestring(imagestr))
