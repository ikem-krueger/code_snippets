#!/bin/sh
# extend non-HiDPI external display on DP* above HiDPI internal display eDP*
# see also https://wiki.archlinux.org/index.php/HiDPI
# you may run into https://bugs.freedesktop.org/show_bug.cgi?id=39949
# https://bugs.launchpad.net/ubuntu/+source/xorg-server/+bug/883319


NUMBER_OF_MONITORS=1
NUMBER_OF_MONITORS=`xrandr | grep " connected" | wc -l`
#echo $NUMBER_OF_MONITORS


if [ "$NUMBER_OF_MONITORS" -eq 2 ] 
then
EXT=`xrandr --current | sed 's/^\(.*\) connected.*$/\1/p;d' | grep -v ^eDP | head -n 1`
INT=`xrandr --current | sed 's/^\(.*\) connected.*$/\1/p;d' | grep -v ^DP | head -n 1`
#echo "EXT:"$EXT
#echo "INT:"$INT
`xrandr --output "${INT}" --auto --output "${EXT}" --auto --scale 2x2 --above "${INT}"`


ext_w=`xrandr | sed 's/^'"${EXT}"' [^0-9]* \([0-9]\+\)x.*$/\1/p;d'`
ext_h=`xrandr | sed 's/^'"${EXT}"' [^0-9]* [0-9]\+x\([0-9]\+\).*$/\1/p;d'`
int_w=`xrandr | sed 's/^'"${INT}"' [^0-9]* \([0-9]\+\)x.*$/\1/p;d'`
off_w=`echo $(( ($int_w-$ext_w)/2 )) | sed 's/^-//'`


#echo "ext_w:"$ext_w
#echo "int_w:"$int_w
`xrandr --output "${INT}" --auto --pos ${off_w}x${ext_h} --scale 0.75x0.75 --output "${EXT}" --auto --scale 2x2 --pos 0x0`






elif [ "$NUMBER_OF_MONITORS" -eq 1 ] 
then
INT=`xrandr --current | sed 's/^\(.*\) connected.*$/\1/p;d' | grep -v ^DP | head -n 1`
#echo $INT
xrandr --output "${INT}" --auto --scale 0.75x0.75
#xrandr --output "${INT}" --auto --scale 1x1

fi
