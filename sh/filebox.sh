#!/bin/bash

# fixes "Failed to initialize JavaFX. Please install JavaFX."

# $ ls -1
# FileBot.jar
# filebot.sh
# javafx-sdk-11.0.2

set -e

# https://stackoverflow.com/questions/59895/get-the-source-directory-of-a-bash-script-from-within-the-script-itself
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

java -p "$DIR/javafx-sdk-11.0.2/lib" --add-modules javafx.swing --add-modules javafx.fxml --add-modules javafx.controls -jar FileBot.jar "$@"