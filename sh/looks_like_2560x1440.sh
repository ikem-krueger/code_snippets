#!/bin/bash
xrandr --output DP-2 --scale-from 5120x2880
gsettings set org.gnome.desktop.interface scaling-factor 2
gsettings set org.gnome.settings-daemon.plugins.xsettings overrides "{'Gdk/WindowScalingFactor': <2>}"

and actually works a bit better on nvidia with eg (different machine, two monitors when I wrote this):


rachel@spitfire:~$ cat bin/looks\ like\ 2560x1440.sh 
nvidia-settings -a CurrentMetaMode="DP-4: nvidia-auto-select +0+0 {viewportin=5120x2880, ForceCompositionPipeline=On, ForceFullCompositionPipeline=On}, DP-2: nvidia-auto-select +5120+0 {viewportin=5120x2880, ForceCompositionPipeline=On, ForceFullCompositionPipeline=On}"
gsettings set org.gnome.desktop.interface scaling-factor 2
gsettings set org.gnome.settings-daemon.plugins.xsettings overrides "{'Gdk/WindowScalingFactor': <2>}"
