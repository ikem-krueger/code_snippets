SELECT time(10, 'unixepoch') # convert secs to HH:MM:SS
SELECT *  FROM Table ORDER BY datetime(datetimeColumn) DESC LIMIT 1 # order by date
SELECT strftime('%d.%m.%Y %H:%M:%S', datetime(moz_bookmarks.dateAdded/1000000, 'unixepoch', 'localtime')) AS datetime, "firefox-bookmarks", moz_bookmarks.title, moz_places.url FROM moz_bookmarks LEFT JOIN moz_places WHERE moz_bookmarks.fk=moz_places.id;
SELECT strftime('%d.%m.%Y %H:%M:%S', datetime(moz_historyvisits.visit_date/1000000, 'unixepoch', 'localtime')) AS datetime, "firefox-history", moz_places.title, moz_places.url FROM moz_places, moz_historyvisits WHERE moz_places.id = moz_historyvisits.place_id;
.backup tablename backup.sqlite
.tables # list the tables in your database
.schema tablename # list how the table looks
.help # list all of the available SQLite prompt commands
.show
.headers on
.mode csv
.output file.txt
.output stdout
