Option Explicit
Const TemporaryFolder = 2
Dim Fso, LastLogon, S
Set S = CreateObject("WScript.Shell")
S.Run "C:\Programme\Netscape\Program\NsNotify.exe"
If MsgBox("Run Calculator?",vbYesNo) = vbYes Then
    S.Run "Calc"
    WScript.Sleep 500   ' Wait 500 milliseconds
    S.SendKeys "%vt"    ' "%vs" if you want scientific mode
End If
On Error Resume Next
LastLogon = S.RegRead("HKCU\LastLogon")
If LastLogon <> CStr(Date) Then
    S.RegWrite "HKCU\LastLogon",Date
    S.Run "C:\Programme\Atomzeit\Atomzeit.exe"
End If
Set Fso = CreateObject("Scripting.FileSystemObject")
Fso.DeleteFile Fso.GetSpecialFolder(TemporaryFolder) & "\*.*",True
