PNET_DISPLAY_USER pBuff, p;
DWORD res, dwRec, i = 0;

do // begin do
{
//
// Call the NetQueryDisplayInformation function;
// specify information level 3 (group account information).
//
res = NetQueryDisplayInformation(NULL, 1, i, 100, MAX_PREFERRED_LENGTH, &dwRec,(PVOID *) &pBuff);
//
// If the call succeeds,
//
if((res==ERROR_SUCCESS) || (res==ERROR_MORE_DATA))
{
p = pBuff;
for(;dwRec>0;dwRec–)
{
//
// Print the retrieved group information.
//
CW2A name( p->usri1_name );
bool bNormalAccount=false;

if(p->usri1_flags & UF_NORMAL_ACCOUNT&& !(p->usri1_flags & UF_ACCOUNTDISABLE)&& !(p->usri1_flags & UF_PASSWD_NOTREQD))
{
bNormalAccount=true;
TRACE(“Normal account %s\n”,name.m_szBuffer);
}

//
i = p->usri1_next_index;
p++;
}
//
// Free the allocated memory.
//
NetApiBufferFree(pBuff);
}
else
printf(“Error: %u\n”, res);
//
// Continue while there is more data.
//
} while (res==ERROR_MORE_DATA); // end do