import Foundation

#if swift(>=4.2)
    // Swift 4.2 already has an Int.random() function
#else
public extension Int {
    public static func random(in range:CountableClosedRange<Int>) -> Int
    {
        return range.lowerBound + Int(arc4random_uniform(UInt32(range.upperBound - range.lowerBound + 1)))
    }
}
#endif
