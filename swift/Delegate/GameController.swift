import UIKit

class GameController: NSObject {
	weak var delegate: ViewController?
	
	func updateText() {
		delegate?.textField.text = "Outch"
	}
}