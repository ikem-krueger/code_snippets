;
; AutoHotkey Version: 1.x
; Language: English
; Platform: Win9x/NT/2k/7
; Author: Ikem Krueger <ikem.krueger@gmail.com>
;
; Script Function: Keynav for Windows
;

CreateWindow(scWidth, scHeight, posX, posY)
{
    #IfWinExist,_keynav
        Gui, destroy
    #IfWinExist

    Gui +AlwaysOnTop -Caption +ToolWindow
    Gui, Color, EEEEEE, _keynav
    Gui, Show, X%A_ScreenWidth% Y%A_ScreenHeight% W%A_ScreenWidth% H%A_ScreenHeight% NoActivate, _keynav
    WinSet, Transparent, 0, _keynav
    WinSet, ExStyle, ^0x20, _keynav
    WinSet, TransColor, EEEEEE, _keynav
    Gui, Show, X0 Y0
}

Draw(_keynav, posX, posY, scWidth, scHeight)
{
    CoordMode, Mouse, Screen
    WinMove, _keynav, , %posX%, %posY%, %scWidth%, %scHeight%
    CrossHair(scWidth, scHeight)
}

Warp(posX, posY, scWidth, scHeight)
{
    CoordMode, Mouse, Screen
    toX := posX + scWidth/2
    toY := posY + scHeight/2
    MouseMove, toX, toY, 0
}

CrossHair(scWidth, scHeight)
{
    Gui, Add, Text, % "x0 y" . scHeight/2 . " w" . scWidth . " h1 0x10 +BackgroundTrans"
    Gui, Add, Text, % "x" . scWidth/2 . " y0 w1 h" . scHeight . " 0x11 +BackgroundTrans"

    Gui, Add, Text, % "x0 y0 w" . scWidth .  " h1 0x10 +BackgroundTrans"
    Gui, Add, Text, % "x0 y0 w1 h" . scHeight . " 0x11 +BackgroundTrans"

    Gui, Add, Text, % "x0 y" . scHeight-1 . " w" . scWidth . " h1 0x10 +BackgroundTrans"
    Gui, Add, Text, % "x" . scWidth-1 . " y0 w1 h" . scHeight . " 0x11 +BackgroundTrans"
}

#SingleInstance force

#IfWinExist,_keynav
    ; Cancel
    Esc::
        Gui, destroy
        return

    ; Window zoom
    w::
        CreateWindow(winWidth, winHeight, winPosX, winPosY)
        scWidth = %winWidth%
        scHeight = %winHeight%
        posX = %winPosX%
        posY = %winPosY%
        Draw(_keynav, posX, posY, scWidth, scHeight)
        Return
    ; Cursor zoom
    c::
        CoordMode, Mouse, Screen
        MouseGetPos, posX, posY
        scWidth = 100
        scHeight = 100
        posX -= scWidth/2
        posY -= scHeight/2
        Draw(_keynav, posX, posY, scWidth, scHeight)
        Return

    ; Move the mouse and left-click
    1::
        Warp(posX, posY, scWidth, scHeight)
        MouseClick, Left
        Gui, destroy
        Return
    ; Move the mouse and double-left-click
    +1::
        Warp(posX, posY, scWidth, scHeight)
        MouseClick, Left, , , 2
        Gui, destroy
        Return
    ; Move the mouse and middle-click
    2::
        Warp(posX, posY, scWidth, scHeight)
        MouseClick, Middle
        Gui, destroy
        Return
    ; Move the mouse and right-click
    3::
        Warp(posX, posY, scWidth, scHeight)
        MouseClick, Right
        Gui, destroy
        Return

    ; Select the left half of the region
    a::
        scWidth /= 2
        Draw(_keynav, posX, posY, scWidth, scHeight)
        Return
    ; Select the right half of the region
    s::
        scWidth /= 2
        posX += %scWidth%
        Draw(_keynav, posX, posY, scWidth, scHeight)
        Return
    ; Select the top half of the region
    d::
        scHeight /= 2
        Draw(_keynav, posX, posY, scWidth, scHeight)
        Return
    ; Select the bottom half of the region
    f::
        scHeight /= 2
        posY += %scHeight%
        Draw(_keynav, posX, posY, scWidth, scHeight)
        Return

    ; Move the region left
    +a::
        posX -= %scWidth%
        Draw(_keynav, posX, posY, scWidth, scHeight)
        Return
    ; Move the region right
    +s::
        posX += %scWidth%
        Draw(_keynav, posX, posY, scWidth, scHeight)
        Return
    ; Move the region up
    +d::
        posY -= %scHeight%
        Draw(_keynav, posX, posY, scWidth, scHeight)
        Return
    ; Move the region down
    +f::
        posY += %scHeight%
        Draw(_keynav, posX, posY, scWidth, scHeight)
        Return

    ; Move the mouse and left-click
    j::
        Warp(posX, posY, scWidth, scHeight)
        MouseClick, Left
        Gui, destroy
        Return
    ; Move the mouse and double-left-click
    +j::
        Warp(posX, posY, scWidth, scHeight)
        MouseClick, Left, , , 2
        Gui, destroy
        Return
    ; Move the mouse and middle-click
    k::
        Warp(posX, posY, scWidth, scHeight)
        MouseClick, Middle
        Gui, destroy
        Return
    ; Move the mouse and right-click
    l::
        Warp(posX, posY, scWidth, scHeight)
        MouseClick, Right
        Gui, destroy
        Return

    ; Move the mouse and left-click
    Space::
        Warp(posX, posY, scWidth, scHeight)
        MouseClick, Left
        Gui, destroy
        Return
    ; Move the mouse and double-left-click
    +Space::
        Warp(posX, posY, scWidth, scHeight)
        MouseClick, Left, , , 2
        Gui, destroy
        Return

    ; Move the mouse and left-click
    Enter::
        Warp(posX, posY, scWidth, scHeight)
        MouseClick, Left
        Gui, destroy
        Return
    ; Move the mouse and double-left-click
    +Enter::
        Warp(posX, posY, scWidth, scHeight)
        MouseClick, Left, , , 2
        Gui, destroy
        Return

    ; Select the left half of the region
    Left::
        scWidth /= 2
        Draw(_keynav, posX, posY, scWidth, scHeight)
        Return
    ; Select the right half of the region
    Right::
        scWidth /= 2
        posX += %scWidth%
        Draw(_keynav, posX, posY, scWidth, scHeight)
        Return
    ; Select the top half of the region
    Up::
        scHeight /= 2
        Draw(_keynav, posX, posY, scWidth, scHeight)
        Return
    ; Select the bottom half of the region
    Down::
        scHeight /= 2
        posY += %scHeight%
        Draw(_keynav, posX, posY, scWidth, scHeight)
        Return

    ; Move the region left
    +Left::
    !Left::
        posX -= %scWidth%
        Draw(_keynav, posX, posY, scWidth, scHeight)
        Return
    ; Move the region right
    +Right::
    !Right::
        posX += %scWidth%
        Draw(_keynav, posX, posY, scWidth, scHeight)
        Return
    ; Move the region up
    +Up::
    !Up::
        posY -= %scHeight%
        Draw(_keynav, posX, posY, scWidth, scHeight)
        Return
    ; Move the region down
    +Down::
    !Down::
        posY += %scHeight%
        Draw(_keynav, posX, posY, scWidth, scHeight)
        Return

    ; Move the mouse and left-click
    Numpad1::
        Warp(posX, posY, scWidth, scHeight)
        MouseClick, Left
        Gui, destroy
        Return
    ; Move the mouse and double-left-click
    +Numpad1::
        Warp(posX, posY, scWidth, scHeight)
        MouseClick, Left, , , 2
        Gui, destroy
        Return
    ; Move the mouse and middle-click
    Numpad2::
        Warp(posX, posY, scWidth, scHeight)
        MouseClick, Middle
        Gui, destroy
        Return
    ; Move the mouse and right-click
    Numpad3::
        Warp(posX, posY, scWidth, scHeight)
        MouseClick, Right
        Gui, destroy
        Return

    ; Move the mouse and left-click
    NumpadEnter::
        Warp(posX, posY, scWidth, scHeight)
        MouseClick, Left
        Gui, destroy
        Return
    ; Move the mouse and double-left-click
    +NumpadEnter::
        Warp(posX, posY, scWidth, scHeight)
        MouseClick, Left, , , 2
        Gui, destroy
        Return

    ; Disable window switcher
    !Tab::
        Return
#IfWinExist

; Desktop zoom
F4::
    ; Save last focussed window
    WinGetActiveStats, winTitle, winWidth, winHeight, winPosX, winPosY

    scWidth = %A_ScreenWidth%
    scHeight = %A_ScreenHeight%
    posX = 0
    posY = 0

    CreateWindow(scWidth, scHeight, posX, posY)
    Draw(_keynav, posX, posY, scWidth, scHeight)
    Return

GuiClose:
    ExitApp
