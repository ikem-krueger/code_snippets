Get-NetNeighbor -LinkLayerAddress 00-15-5d-* # get IP address of all virtual machines
Set-VHD -Path 'X:\\Virtual Machines\\Virtual Hard Disks\\VM01.vhdx' \` -ParentPath "X:\\Server2012Template\\Virtual Hard Disks\\Server2012Template.vhdx"
Get-WindowsOptionalFeature -Online -FeatureName Microsoft-Hyper-V
Get-Command -Module "Hyper-V"
Set-VMVideo -VMName "Ubuntu 20.04" -HorizontalResolution 2560 -VerticalResolution 1440 -ResolutionType Single
Start-VM "Windows 8.1 Pro" -Computername HV-Host1
Stop-VM "Windows 8.1 Pro" -Save # save or shutdown virtual machine
Get-VM | Select VMName, VMId # find the guid of a virtual machine
Get-VM -Name "Ubuntu 20.04.1 Desktop" | Select *
Get-VM -Name "Linux Mint 20.2 XFCE" | Select -ExpandProperty Networkadapters
Get-VM | ?{$_.State -eq "Running"} | Select -ExpandProperty Networkadapters | Select VMName, MacAddress, SwitchName, IPAddresses | ft -wrap-autosize # get ip addresses of all virtual machines
Get-VM | Where{$_.State -eq "Saved"} | Start-VM -Passthru | Stop-VM # shutdown all saved virtual machines
Get-VM | Where{$_.Uptime -gt [timespan]"180.00:00:00"} | Restart-VM # restart virtual machines depending on their uptime
Get-VMFirmware "Ubuntu 20.04.1 Desktop"
Move-VMStorage "Test VM" -DestinationStoragePath D:\TestVM
Move-VMStorage "Ubuntu 20.04.1 Desktop" -SnapshotFilePath X:\Hyper-V
Move-VMStorage "MyVM1" -VirtualMachinePath E:\MyVM1\VMConfigFile -SnapshotFilePath E:\MyVM1\SnapShotFiles -SmartPagingFilePath E:\MyVM1\SmartPaging
Move-VMStorage "Test VM" -VirtualMachinePath D:\TestVM\Config -SnapshotFilePath D:\TestVM\Snapshots -SmartPagingFilePath D:\TestVM\SmartPaging -VHDs @(@{"SourceFilePath" = "C:\TestVM\Disk1.VHDX"; "DestinationFilePath" = "D:\TestVM\Disks\Disk1.VHDX"}, @{"SourceFilePath" = "C:\TestVM\Disk2.VHDX"; "DestinationFilePath" = "D:\TestVM\Disks\Disk2.VHDX"})
