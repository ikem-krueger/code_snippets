docker container stop basexhttp
docker container rm basexhttp
docker build # builds an image from a dockerfile
docker tag # tags an image
docker images # lists images
docker run # runs a container based on an image
docker run -it <container> bash
docker run -it --name my-container --rm openjdk:8-jdk-slim # throw away container
docker push # pushes an image to a registry
docker pull # pulls an image from a repository
docker ps # lists containers
docker system prune # remove unused containers and images
docker stats
