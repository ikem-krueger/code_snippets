![](https://upload.wikimedia.org/wikipedia/commons/thumb/e/e1/Hyperviseur.png/400px-Hyperviseur.png)

| Type | Example | Operating System |
| -- | -- | -- |
| Type 1 | Hyper-V | Windows |
| Type 1 | ESX/ESXi, Xen, XenServer, KVM and OpenVZ | Linux |
| Type 2 | VirtualBox, VMWare | Windows, Linux |
