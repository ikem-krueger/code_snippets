## Types vs. Instances

A type is like a generic descriptor for a concept and an instance is an item described by a type.

Where possible, UML indicates this distinction by underlining the name for instances.

Examples:

* Class vs. Object
* Association vs. Link
* Node vs. Node instance
* Parameter vs. Value
* Operation vs. Call
* Use case vs. Scenario 
